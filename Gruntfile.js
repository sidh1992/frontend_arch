// Initialize Grunt configuration
module.exports = function(grunt){
    // Initialize default task
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        /**
        * Watch function lookout the file which modify when server is running
        * Based on configuration respected task will be run
        */
        watch: {
            jsFiles: {
                files: ['app/*.js', 'app/**/*.js', 'app/**/**/*.js'],
                tasks: ['concat:jsFiles', 'concat:jsLib']
            },
            cssFiles: {
                files: ['css/*.css', 'css/**/*.css'],
                tasks: ['concat:cssFiles', 'concat:cssLib']
            },
            configFiles: {
                files: ['Gruntfile.js'],
                tasks: [
                    'concat:jsFiles', 'concat:jsLib', 'concat:cssFiles',
                    'concat:cssLib'
                ]
            }
        },
        /**
        * Concat application related file based on file type and location
        * Based on configuration source file concat to destination files
        * Note :- Make sure source file will arrange in order.
        */
        concat: {
            jsFiles: {
                options: {
                    separator: '\n;'
                },
                src: [
                    'app/app.module.js',
                    'app/app.constant.js',
                    'app/app.constant.js',
                    'app/app.factory.js',
                    'app/app.service.js',
                    'app/app.router.js',
                    'app/app.filter.js',
                    'app/header/headerController.js',
                    'app/footer/footerController.js',
                    'app/carousel/mainSliderController.js',
                    'app/login/loginController.js',
                    'app/register/registerController.js'
                ],
                dest: 'build/app/js/app.js'
            },
            cssFiles: {
                options: {
                    separator: '\n'
                },
                src: [
                    'css/app.css'
                ],
                dest: 'build/app/css/app.css'
            },
            jsLib: {
                options: {
                    separator: '\n;'
                },
                src: [
                    'node_modules/jquery/dist/jquery.min.js',
                    'node_modules/angular/angular.min.js',
                    'node_modules/angular-ui-router/release/angular-ui-router.js',
                    'node_modules/satellizer/dist/satellizer.min.js',
                    'node_modules/bootstrap/dist/js/bootstrap.min.js'
                ],
                dest: 'build/lib/app.lib.js'
            },
            cssLib: {
                options: {
                    separator: '\n'
                },
                src: [
                    'node_modules/bootstrap/dist/css/bootstrap.min.css'
                ],
                dest: 'build/lib/app.lib.css'
            }
        },
        browserSync: {
            test: {
                bsFiles: {
                    src: [
                        '*.html',
                        'css/*.css',
                        'css/**/*.css',
                        'js/**/*.js',
                        'app/*.js',
                        'app/**/*.html',
                        'app/**/*.js',
                        'app/**/**/*.js',
                        'app/**/**/*.html',
                        'app/**/**/**/*.html',
                        'Gruntfile.js'
                    ]
                },
                options: {
                    watchTask: true,
                    debounceDelay: 1000,
                    debugInfo : true,
                    server: {
                        baseDir: './',
                        index: 'index.html'
                    }
                }
            }
        }
    });

    // Load Grunt plugins
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browser-sync');

    // Default task(s)
    grunt.registerTask('default', ['browserSync', 'watch','concat']);
    grunt.registerTask('build', ['concat']);
}
