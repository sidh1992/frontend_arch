(function(angular){

    "use strict";


    function factoryFunction($http, $rootScope){
        var domain = $rootScope.API_DOMAIN;

        function get_data(URL, data){
            console.info(domain);
            console.info(domain+URL);
            return $http.get(domain+URL)
                .then(getSuccessResponse)
                .catch(getFailureResponse);

            function getSuccessResponse(response){
                return response;
            }

            function getFailureResponse(response){
                return response;
            }
        }

        function post_data(URL, data){
            return $http.post(domain+URL, data)
                .then(getSuccessResponse)
                .catch(getFailureResponse);

            function getSuccessResponse(response){
                return {
                    'data':response,
                    'success':true
                };
            }

            function getFailureResponse(response){
                return {
                    'data':response,
                    'success':false
                };
            }
        }

        return {
            get: get_data,
            post: post_data
        }
    }

    factoryFunction.$inject = ['$http', '$rootScope'];

    angular.module('frontendArch')
        .factory('factoryFunction', factoryFunction);
})(window.angular);
