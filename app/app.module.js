(function(angular){
    "use strict";

    angular.module('frontendArch', [
        'ui.router',
        'satellizer'
    ]);

})(window.angular);
