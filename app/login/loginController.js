(function(angular){
    "use strict";

    function loginController($http, $scope, serviceFunction){
        $scope.username = '';
        $scope.password = '';

        $scope.loginSubmit = function(){
            response = serviceFunction.login({
                'username':$scope.username,
                'password':$scope.password
            });
            if (!response.error) {
                alert("login");
            }

        }
    }

    angular.module('frontendArch')
        .controller('loginController', ['$http', '$scope', 'serviceFunction', loginController]);
})(window.angular);
