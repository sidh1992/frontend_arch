(function(angular){
    "use strict";

    angular.module("frontendArch")
        .constant('API_DOMAIN', 'http://127.0.0.1:8000/')
        .constant('IMAGE_DOMAIN', 'https://www.lokafy.com/static/')
        .constant(
            "MAINSLIDERS", [
                {
                    "image_path":"images/city/Mumbai.png",
                    "status":true,
                    "description":"test",
                    "link":"https://www.google.com",
                    "name": "1",
                    "display_first": "active"
                },
                {
                    "image_path":"images/city/delhi.png",
                    "status":false,
                    "description":"test",
                    "link":"https://www.google.com",
                    "name": "2",
                    "display_first": ""
                },
                {
                    "image_path":"images/city/paris.png",
                    "status":true,
                    "description":"test",
                    "link":"https://www.google.com",
                    "name": "3",
                    "display_first": ""
                },
                {
                    "image_path":"images/city/toronto.png",
                    "status":true,
                    "description":"test",
                    "link":"https://www.google.com",
                    "name": "4",
                    "display_first": ""
                }
            ]
        )
        .constant('API',{
            'REGISTRATION':'privacy/',
            'LOGIN':'account/login/',
            'LOGOUT':'account/logout/'
        })
        .run(function ($rootScope, API_DOMAIN, IMAGE_DOMAIN, MAINSLIDERS) {
            $rootScope.API_DOMAIN = API_DOMAIN;
            $rootScope.IMAGE_DOMAIN = IMAGE_DOMAIN;
            $rootScope.MAINSLIDERS = MAINSLIDERS;
        });

})(window.angular);


