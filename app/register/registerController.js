(function(angular){
    "use strict";

    function registerController($http, $scope, $auth, factoryFunction, API) {

        $scope.name = '';
        $scope.email = '';
        $scope.username = '';
        $scope.password = '';
        $scope.repassword = '';
        $scope. registerSubmit = registerSubmit;

        $scope.authenticate = function(provider) {
          $auth.authenticate(provider);

        };

        function registerSubmit(){
            if ($scope.password != $scope.repassword) {

            }

            var params = {
                'name':$scope.name,
                'email':$scope.email,
                'username':$scope.username,
                'password':$scope.password,
                'repassword':$scope.repassword
            }

            factoryFunction.post(API.REGISTRATION, params)
                .then(function(response){
                    console.info(response);
                    if (response.success) {

                    }
                });
        }

    }

    registerController.$inject = [
        '$http', '$scope', '$auth', 'factoryFunction', 'API'
    ];

    angular.module('frontendArch')
        .controller('registerController', registerController);

})(window.angular);
