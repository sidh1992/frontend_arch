(function(angular){
    "use strict";

    angular.module('frontendArch')
        .config(function($authProvider){

            $authProvider.facebook({
                clientId: 'Facebook ID'
            });

            $authProvider.google({
                clientId: 'Google ID'
            });

            $authProvider.linkedin({
                clientId: 'Linkedin ID'
            });

        });
})(window.angular);
