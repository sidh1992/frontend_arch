(function(angular){
    "use strict";

    function mainSliderController($http, $scope, $rootScope, factoryFunction) {
    };

    angular.module('frontendArch').controller(
        'mainSliderController', [
        '$http', '$scope', 'factoryFunction','$rootScope', mainSliderController]
    );
})(window.angular);
