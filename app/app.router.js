(function(angular){
    "use strict";

    angular.module('frontendArch')
        .config(config);

    function config($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider, $provide){
        // Initial template
        var initTemplateUrl = 'app/index.html';

        // the known route
        $urlRouterProvider.when('', '/');

        // For any unmatched url, redirect to /404
        $urlRouterProvider.otherwise("/404");

        $stateProvider
            .state('/', {
                url: '/',
                templateUrl: function(params) {
                    return initTemplateUrl;
                }
            })
            .state('faq', {
                url: '/faq',
                templateUrl: 'app/static/faq-template.html'
            })
            .state('privacy-policy', {
                url: '/privacy-policy',
                templateUrl: 'app/static/privacy-policy-template.html'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'app/static/about-template.html'
            })
            .state('review', {
                url: '/review',
                templateUrl: 'app/static/reviews-template.html'
            })
            .state('contact', {
                url: '/contact-us',
                templateUrl: 'app/static/contact-us-template.html'
            })
            .state('press', {
                url: '/press',
                templateUrl: 'app/static/press-template.html'
            })
            .state('pricing', {
                url: '/pricing',
                templateUrl: 'app/price/price-template.html',
                controller: 'priceController'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'app/register/register-template.html',
                controller: 'registerController'
            });
    }

})(window.angular);
