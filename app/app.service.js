(function(angular){
    "use strict";

    function serviceFunction($http, $q, factoryFunction){

        this.login = function(param){
            var deferred = $q.defer();
            factoryFunction.post('/account/login/', param)
                .then(function(data){
                    deferred.resolve(data);
                });

                return deferred.promise;
        }

    }

    angular.module('frontendArch')
        .service('serviceFunction', ['$http', '$q', 'factoryFunction',serviceFunction]);
})(window.angular);
