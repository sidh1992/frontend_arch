# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is help developer to start fronted project with angular, bootstrap and grunt. This repository
* is a boilerplate for frontend integrations. Some basic angular file structure, bootstrap settings & Grunt configuration
* included in this repository.
* Version 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone repository in your system (To start with different project name just mention `<project_name>` after git clone url)
* You should have node & npm install in your system 
* Once you clone the repo, open repo in commandline prompt 
* Inside the repository packge file is available. To install dependancies run following command
	`npm install`
* note :- Some time require admin permission to install dependacies. In that case add `sudo` first and then paste same command 
* Once dependacies install you can run your code by writing `grunt` in command line

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact